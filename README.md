# Rori Kurniadi Test
This is simple auth build with golang and VueJs

# Golang API
## Installation
go get bitbucket.org/rorikurniadi/rori_kurniadi_test

## Config
modify config in configs directory. (configs/config)

## Run Services
```
go run main.go
```

# Front End Strategy with VueJs
## Installation
```
cd views
npm install
```

## Config
You need modify endpoint golang in views/src/services/main.VueJs

## Run
```
npm run dev
```

# Demo
this is demo [SimpleAuth] (http://139.59.98.7/)